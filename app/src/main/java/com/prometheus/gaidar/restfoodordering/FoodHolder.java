package com.prometheus.gaidar.restfoodordering;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

/**
 * Created by Andrew on 15.11.2017.
 */


class FoodHolder extends RecyclerView.ViewHolder {
    View line;
    TextView name, count;
    View counter;

    public FoodHolder(View itemView) {
        super(itemView);
        line = itemView.findViewById(R.id.line);
        name = itemView.findViewById(R.id.tv_name);
        count = itemView.findViewById(R.id.tv_count);
        counter = itemView.findViewById(R.id.counter);

    }

    void bind(Food food) {
        name.setText(food.getName());
        count.setText(food.getCount() + "шт.");
    }
    void showCounter(){
       count.setVisibility(View.INVISIBLE);
       counter.setVisibility(View.VISIBLE);
    }
    void hideCounter(){
        count.setVisibility(View.INVISIBLE);
        counter.setVisibility(View.VISIBLE);
    }

}