package com.prometheus.gaidar.restfoodordering;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrew on 14.11.2017.
 */

public class ListFoodFragment extends Fragment implements View.OnClickListener {


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_food_list, container, false);
        List<Food> list = new ArrayList<>();
        Food food1 = new Food(35, "Вино", 5);
        list.add(food1);
        Food food2 = new Food(56, "Масло", 13);
        list.add(food2);
        Food food3 = new Food(11, "Гречка", 15);
        list.add(food3);

        ExpandableListView expandable = view.findViewById(R.id.expandable);
        FoodListAdapter adapter = new FoodListAdapter(getActivity(), list);
        FoodAdapterWithAdd adapterWithAdd = new FoodAdapterWithAdd(list, getActivity());
        RecyclerView recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(),2));
        recyclerView.setAdapter(adapterWithAdd);
        expandable.setAdapter(new ExpandableAdapter(getContext(), expandable, 2, adapter));
        return view;
    }

    private void showMenu() {
        MenuBottomDialogFragment fragment = new MenuBottomDialogFragment();
        fragment.show(getChildFragmentManager(), "Menu");
    }


    @Override
    public void onClick(View v) {
        showMenu();
    }
}
