package com.prometheus.gaidar.restfoodordering;



import android.graphics.Color;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrew on 14.11.2017.
 */

public class Util {
    public static String[] getMaterialColors() {
        String[] colorsCode = new String[]{"#F44336", "#9C27B0", "#3F51B5", "#E91E63"};

        return colorsCode;
    }
}
