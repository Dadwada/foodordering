package com.prometheus.gaidar.restfoodordering;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CompoundButton;
import android.widget.ExpandableListView;
import android.widget.ToggleButton;

/**
 * Created by Andrew on 15.11.2017.
 */

public class ExpandableAdapter extends BaseExpandableListAdapter {
    Context mContext;
    ExpandableListView mExpandableListView;
    int gridCount;
    FoodListAdapter mAdapter;

    public ExpandableAdapter(Context mContext, ExpandableListView expandableListView, int span, FoodListAdapter adapter) {
        this.mContext = mContext;
        mExpandableListView = expandableListView;
        gridCount = span;
        mAdapter = adapter;
    }

    @Override
    public int getGroupCount() {
        return 1;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return null;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {

        return new Object();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.content_expand, null, false);
        ToggleButton toggleButton = view.findViewById(R.id.image);
        toggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    mExpandableListView.expandGroup(0);
                else
                    mExpandableListView.collapseGroup(0);
            }
        });
        return view;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        RecyclerView recyclerView = (RecyclerView) LayoutInflater.from(mContext).inflate(R.layout.child_content, null, false);

        recyclerView.setLayoutManager(new GridLayoutManager(mContext, gridCount));
        recyclerView.setAdapter(mAdapter);
        return recyclerView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
