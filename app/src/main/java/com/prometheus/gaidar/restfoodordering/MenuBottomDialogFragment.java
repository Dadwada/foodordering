package com.prometheus.gaidar.restfoodordering;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.View;
import android.widget.TextView;

/**
 * Created by Andrew on 15.11.2017.
 */

public class MenuBottomDialogFragment extends BottomSheetDialogFragment {

    TextView tvChangeCount,tvRemove,tvCancel;
    @SuppressLint("RestrictedApi")
    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        View contentView = View.inflate(getContext(), R.layout.bottom_menu, null);
        tvChangeCount = contentView.findViewById(R.id.tv_changeCount);
        tvChangeCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        tvCancel = contentView.findViewById(R.id.tv_cancel);
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        dialog.setContentView(contentView);
    }
    public interface ChangeCountListener{
        void onClickChanged();
    }
}
