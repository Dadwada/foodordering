package com.prometheus.gaidar.restfoodordering;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

/**
 * Created by Andrew on 15.11.2017.
 */

public class FoodAdapterWithAdd extends RecyclerView.Adapter<FoodHolder> {
    private List<Food> foodList;
    private Context mContext;
private View.OnClickListener listener = new View.OnClickListener() {
    @Override
    public void onClick(View v) {

    }
};

    public void setListener(View.OnClickListener listener) {
        this.listener = listener;
    }

    public FoodAdapterWithAdd(List<Food> foodList, Context mContext) {
        this.foodList = foodList;
        this.mContext = mContext;
    }

    private static int NORMAL_ITEM = 0;
    private static int ADD_ITEM = 1;

    @Override
    public FoodHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (viewType == NORMAL_ITEM) {

            view = LayoutInflater.from(mContext).inflate(R.layout.food_list_item, parent, false);
            view.findViewById(R.id.iv_show_menu).setOnClickListener(listener);
        }
        else
            view = LayoutInflater.from(mContext).inflate(R.layout.add_content,parent,false);
        return new FoodHolder(view);

    }

    @Override
    public void onBindViewHolder(FoodHolder holder, int position) {
        if (getItemViewType(position)==NORMAL_ITEM)
            holder.bind(foodList.get(position));
    }

    @Override
    public int getItemCount() {
        return foodList.size()+1;
    }

    @Override
    public int getItemViewType(int position) {
        if (getItemCount()-1==position){
            return ADD_ITEM;
        }
        return NORMAL_ITEM;
    }
}
