package com.prometheus.gaidar.restfoodordering;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrew on 14.11.2017.
 */

public class FoodListAdapter extends RecyclerView.Adapter<FoodHolder> {
    List<Food> list;
    private View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

        }
    };

    public void setListener(View.OnClickListener listener) {
        this.listener = listener;
    }

    private Context mContext;
    private String[] colors;

    public FoodListAdapter(Context context, List<Food> foodList) {
        this.mContext = context;
        colors = Util.getMaterialColors();
        list = foodList;


    }

    @Override
    public FoodHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.food_list_item, parent, false);
        View menuIcon = view.findViewById(R.id.iv_show_menu);
        menuIcon.setOnClickListener(listener);

        return new FoodHolder(view);
    }

    @Override
    public void onBindViewHolder(FoodHolder holder, int position) {
        int colorPos = position % colors.length;
        int color = Color.parseColor(colors[colorPos]);
        holder.bind(list.get(position));
        Log.i("Colors", color + "");
        holder.line.setBackgroundColor(color);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


}
