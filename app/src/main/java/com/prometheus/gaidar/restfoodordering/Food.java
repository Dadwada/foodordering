package com.prometheus.gaidar.restfoodordering;

/**
 * Created by Andrew on 15.11.2017.
 */

public class Food {
    private int prise;
    private String name;
    private float count;

    public Food(int prise, String name, float count) {
        this.prise = prise;
        this.name = name;
        this.count = count;
    }
    public void increase(float value){
        count+=value;
    }
    public void dincrease(float value){
        count-=value;
    }

    public int getPrise() {
        return prise;
    }

    public String getName() {
        return name;
    }

    public float getCount() {
        return count;
    }
}
